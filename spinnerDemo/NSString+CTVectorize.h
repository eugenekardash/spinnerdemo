//
//  NSString+CTVectorize.h
//  spin2
//
//  Created by Eugene Kardash on 6/19/14.
//  Free license.
//

#import <Foundation/Foundation.h>

@interface NSString (CTVectorize)

- (void)doSomething;

@end
