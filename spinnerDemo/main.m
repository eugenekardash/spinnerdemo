//
//  main.m
//  spin2
//
//  Created by Eugene Kardash on 6/19/14.
//  Free license.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
