//
//  CTAppDelegate.h
//  spin2
//
//  Created by Eugene Kardash on 6/19/14.
//  Free license.
//

#import <Cocoa/Cocoa.h>

@interface CTAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
