//
//  CTAppDelegate.m
//  spin2
//
//  Created by Eugene Kardash on 6/19/14.
//  Free license.
//

#import "CTAppDelegate.h"
#import "NSString+CTVectorize.h"

@interface CTAppDelegate ()
@property (weak) IBOutlet NSProgressIndicator *big;
@property (weak) IBOutlet NSProgressIndicator *small;
@property (weak) IBOutlet NSButton *button;
@end

@implementation CTAppDelegate
{
    __weak NSProgressIndicator *_big;
    __weak NSProgressIndicator *_small;
    __weak NSButton *_button;
    
    BOOL started;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    started = NO;
}

- (IBAction)pressed:(id)sender {
    if(!started) {
        [_big startAnimation:self];
        [_small startAnimation:self];
    } else {
        [_big stopAnimation:self];
        [_small stopAnimation:self];
    }
    
    started = !started;
    
    [@"string" doSomething];
}


@end
